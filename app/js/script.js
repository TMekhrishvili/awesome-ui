const hamburger = document.querySelector('#hamburger');
const header = document.querySelector('.header');
const hasFade = document.querySelectorAll('.has-fade');
const body = document.querySelector('body');

hamburger.addEventListener('click', function () {
    if (header.classList.contains('open')) { // close humburger
        body.classList.remove('noscroll')
        header.classList.remove('open');
        hasFade.forEach(function (element) {
            element.classList.remove('fade-in');
            element.classList.add('fade-out');
        })
    }
    else { // open hamburger
        body.classList.add('noscroll')
        header.classList.add('open')
        hasFade.forEach(function (element) {
            element.classList.remove('fade-out');
            element.classList.add('fade-in');
        })
    }
})